#!/bin/bash


echo "##### Info CPU #####"
echo
lscpu
echo

echo "##### Info Memoria #####"
echo
free -h
echo

echo "##### Info de dispositivo de bloco #####"
echo
lsblk
echo

echo "##### Dipositivos PCI #####"
echo
lspci
echo

echo "##### Dispositivos USB #####"
echo
lsusb
echo

echo "##### Placa mae #####"
echo
sudo dmidecode -t baseboard 2>/dev/null
