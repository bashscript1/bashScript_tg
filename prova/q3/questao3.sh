#/bin/bash

red='\033[0;31m'
nc='\033[0m'

echo "No bash, exitem algumas formas de criar variaveis. Sao elas:"
echo -e "Voce pode criar uma variavel atribuindo um valor a ela. Ex: ${red}a=\"um valor qualquer\"${nc}"

echo -e "Voce pode criar uma varialvel lendo a entrada padrao com a keyword read. Ex: ${red}read a${nc}"

echo -e "Voce tambem pode criar uma variavel usando atribuicao de comando. EX: ${red}data=\$(date +%Y/%m/%d)${nc}"

echo -e "Voce tambem pode criar uma de ambiente da seguinte forma: ${red}export MY_VAR=\"valor\"${nc}"

echo -e "Exitem tambem as variaveis posicionais, que sao as que voce pode receber como argumente de um script. Ex: ${red}./meubash.sh valor1 valor2${nc}"
echo "Nesse caso valor1 e valor2 serao as variaveis posicionais ${red}\$1 e \$2${nc} dentro do script"

echo -e "Existe ainda as variaveis especiais ou automaticas que sao as variaveis ${red}\$?${nc} que contem o codigo de retorno do ultimo comando executado."
echo -e "Outras exemplos sao ${red}\$#${nc} que indica o numero total de argumentos passados para o script."
echo "Existem muitas outras."


echo "A diferença entre pedir explicitamente um valor e passa-lo como paramentro, esta no seguinte."
echo -e "Quando se pede explicitamente um valor com o read, esse valor pode ser passado em qualquer fase do programa, tanto no inicio como no fim. Ja como parametro, obrigatoriamente deve ser passado na hora da execucao do script, na forma: ${red}./meuprograma.sh param1 param2${nc}"

