#!/bin/bash

cor_verde='\033[0;32m'
cor_amarela='\033[0;33m'
cor_azul='\033[0;34m'
cor_roxa='\033[0;35m'
cor_ciano='\033[0;36m'

echo -e "${cor_roxa}A noite se veste de estrelas douradas,"
sleep 2
echo -e "${cor_ciano}E o silêncio acalenta a alma cansada,"
sleep 2
echo -e "${cor_amarela}Na penumbra, segredos sussurrados,"
sleep 2
echo -e "${cor_azul}O coração dança na dança da madrugada,"
sleep 2
echo -e "${cor_verde}Em cada sombra, uma história entrelaçada."
